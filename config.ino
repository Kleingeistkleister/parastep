

// sets the step mode, choosen by DIP switches on motor controller
long stepMode(int y) {
  if (y == 1) {
    steps = 6400;
  } else if (y == 2) {
    steps = 3200;
  } else if (y == 3) {
    steps = 1600;
  } else if (y == 4) {
    steps = 800;
  } else if (y == 5) {
    steps = 400;
  } else if (y == 6) {
    steps = 400;
  } else if (y == 7) {
    steps = 200;
  }
  stepAngle = (steps) / 90;
  return steps;

}


//Stepper setups
void stepperSetup() {
  stepper1.setAcceleration((steps/8));
  stepper2.setAcceleration((steps/8));
  stepper3.setAcceleration((steps/8));
  stepper4.setAcceleration((steps/8));
  stepper5.setAcceleration((steps/8));
  stepper6.setAcceleration((steps/8));

  stepper1.setMaxSpeed(steps*2);
  stepper2.setMaxSpeed(steps*2);
  stepper3.setMaxSpeed(steps*2);
  stepper4.setMaxSpeed(steps*2);
  stepper5.setMaxSpeed(steps*2);
  stepper6.setMaxSpeed(steps*2);

  steppers.addStepper(stepper1);
  steppers.addStepper(stepper2);
  steppers.addStepper(stepper3);
  steppers.addStepper(stepper4);
  steppers.addStepper(stepper5);
  steppers.addStepper(stepper6);
}

void setAllAcceleration(long vel) {

  stepper1.setAcceleration(vel);
  stepper2.setAcceleration(vel);
  stepper3.setAcceleration(vel);
  stepper4.setAcceleration(vel);
  stepper5.setAcceleration(vel);
  stepper6.setAcceleration(vel);
}

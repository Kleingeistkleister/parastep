//checks if saving mode is active on channel 12.
boolean saveMode() {
  if (dmxValues[11] == 255) {
    return true ;
  } else
    return false;
}//saveMode



//let the steppers rotate in diffrent speeds
void rotateAll() {
  float speeds[6];
  steppers.isRotated(steps);
  calculateSpeed(speeds, dmxValues, lastDmxValues, 6);
  steppers.setSpeed(speeds);
  steppers.runSpeed();
}

//reads out DMX channels
void readDmx(int *dmxVal, size_t count) {
  for (size_t i = 0; i < count; i++) {
    dmxVal[i] = dmx_slave.getChannelValue (i + 1);
  }
}


// calculates angle from DMX value to steps
void calculatePositions(long *positions, const int *dmxVal, size_t count) {
  for (size_t i = 0; i < count; i++) {

    if (dmxVal[i] > 138 ) {
      positions[i] = ((dmxVal[i] - 138) * 90 / 117 * stepAngle );

    } else if  (dmxVal[i] < 117) {
      positions[i] = ((dmxVal[i] - 117) * 90 / 117 * stepAngle );
    } else {
      positions[i] = 0;
    }
  }
}



//reads out DMX Value and calculates the speed for stepper. Sets last speed if DMX value not changed.
void calculateSpeed(float *velocity, const int *dmxSpeed, int *lastDMX, size_t count) {


  for (size_t i = 0; i < count; i++) {
    if (dmxSpeed != lastDMX[i]) { // check if dmx has changed.
      if (dmxSpeed[i] > 138 ) {
        velocity[i] = (dmxSpeed[i] - 138) * 40;
        lastDMX[i] = dmxSpeed[i];
      }
      else if  (dmxSpeed[i] < 117) {

        velocity[i] = ((dmxSpeed[i] - 117) * 40);
        lastDMX[i] = dmxSpeed[i];
      }
      else {
        velocity[i] = 0;
        lastDMX[i] = dmxSpeed[i];
      }
    }
  }
}//calcSpeed


//builds a String for each line on the display
//void outputLCD(){
//strcpy line0 = {stepper1.currentPosition()," ",stepper3.currentPosition()" "stepper5.currentPosition()};
//strcpy line1 ={ stepper2.currentPosition()," ",stepper4.currentPosition()" "stepper6.currentPosition()};
//}



//saves all current positions to an array
long allCurrentPos() {
  currentPos[0] = stepper1.currentPosition();
  currentPos[1] = stepper2.currentPosition();
  currentPos[2] = stepper3.currentPosition();
  currentPos[3] = stepper4.currentPosition();
  currentPos[4] = stepper5.currentPosition();
  currentPos[5] = stepper6.currentPosition();
}

//reads DMX Value and moves to saved position.
void runPositions() {
  //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  //position 1
  //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  if (posP  >= 0 && posP < 32) {
    steppers.moveTo(angle90);
  }
  //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  //position 2
  //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  if (posP  >= 32 && posP < 64) {
    steppers.moveTo(angle45);
  }

  //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  //position 5
  //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  if (posP  >= 64 && posP < 96) {
    steppers.moveTo(angle0);

  }
  //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  //position 6
  //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  if (posP  >= 96 && posP < 128) {
    steppers.moveTo(angle45N);

  }

  //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  //position 7
  //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  if (posP  >= 128 && posP < 160) {
    steppers.moveTo(angle90N);
  }
  //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  //position 8
  //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  if (posP  >= 160 && posP < 192) {
    steppers.moveTo(position1);
  }
  //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  //position 8
  //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  if (posP  >= 192 && posP < 224) {
    steppers.moveTo(position2);
  }
  //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  //position 8
  //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  if (posP  >= 224 && posP < 255) {
    steppers.moveTo(position3);
  }
}


// saves data to EEprom
void savePosToEEProm(long *currentPos){
  
}

void outputLCD() {
    char line0[17];
    char line1[17];

    snprintf(line0, sizeof(line0), "%ld %ld %ld",
             stepper1.currentPosition(), stepper3.currentPosition(), stepper5.currentPosition());
    snprintf(line1, sizeof(line1), "%ld %ld %ld",
             stepper2.currentPosition(), stepper4.currentPosition(), stepper6.currentPosition());
}

void updateLCD() {
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print(line0);
  lcd.print(line1);
}


//sets all stepppers to pos 0

void setZero() {
  stepper1.setCurrentPosition(0);
  stepper2.setCurrentPosition(0);
  stepper3.setCurrentPosition(0);
  stepper4.setCurrentPosition(0);
  stepper5.setCurrentPosition(0);
  stepper6.setCurrentPosition(0);
}

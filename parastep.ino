//Written by Torben Schloss



//*******************************
// includes
//*******************************
#include <AccelStepper.h>     // stepper libary by Mike McCauley
#include <MultiStepper.h>
#include <LiquidCrystal.h>
#include <Conceptinetics.h>


//*******************************
// steppers
//*******************************
// Array der jetzigen Motoren.
const size_t motors = 6;
long stepperSpeed = 0;
long stepAngle = 0;
long steps = 0;

boolean bRun;
int  program; //program pointer
int savePointer; // save pointer
int  posP;
long LCDTimer = 0;
long LCDTimeout = 1000;



float lastSpeed[6];

float speeds[6];

//*******************************
// DMX
//*******************************
#define DMX_SLAVE_CHANNELS   12
#define RXEN_PIN     2
DMX_Slave dmx_slave ( DMX_SLAVE_CHANNELS ,  RXEN_PIN );
int dmxValues[DMX_SLAVE_CHANNELS];
int lastDmxValues[DMX_SLAVE_CHANNELS];


//*******************************
// positions
//*******************************
//Arrays with 6 datas for 6 motors


long dmxAngle[6];
long currentPos[6];
//fixed angle positions
long angle0[6];
long angle45[6];
long angle90[6];
long angle45N[6];
long angle90N[6];


long position1[6];
long position2[6];
long position3[6];
long position4[6];


// Define a stepper and the pins it will use
AccelStepper stepper1(AccelStepper::FULL2WIRE, 30, 31);
AccelStepper stepper2(AccelStepper::FULL2WIRE, 32, 33);
AccelStepper stepper3(AccelStepper::FULL2WIRE, 34, 35);
AccelStepper stepper4(AccelStepper::FULL2WIRE, 36, 37);
AccelStepper stepper5(AccelStepper::FULL2WIRE, 38, 39);
AccelStepper stepper6(AccelStepper::FULL2WIRE, 40, 41);

//Stepper arrays
MultiStepper steppers;


//*******************************
//LCD pins
//*******************************

const int rs = 8, en = 9, d4 = 4, d5 = 5, d6 = 6, d7 = 7;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);

char line0[17];
char line1[17];
//some timers.
long lcdTimer = 0;
long updateTime = 1000;



//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//                            Setup
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void setup()
{




  //*******************************
  // LCD setups
  //*******************************
  // set up the LCD's number of columns and rows:
  lcd.begin(16, 2);


  //*******************************
  // DMX setups
  //*******************************
  // Enable DMX slave interface and start recording
  // DMX data
  dmx_slave.enable ();

  // Set start address to 1, this is also the default setting
  // You can change this address at any time during the program
  dmx_slave.setStartAddress (1);

  //****s***************************
  // stepper setups
  //*******************************
  //Check Stepmode with DIP switches on stepper controller
  //returns how many steps are 90°
  stepMode(1);

  //Sets maxSpeed and Acceleration
  stepperSetup();

  long dmxAngle[6];
  //  All steppers to Zero
  long  angle0[6] = {0 , 0 , 0 , 0 , 0 , 0 };

  //Calculates the steps for angle.
  long dX;

  //45°
  dX = 0.5 * steps;
  long angle45[6] = {dX, dX, dX, dX, dX, dX};

  // 90°
  dX = steps;
  long  angle90[6]  = {dX, dX, dX, dX, dX, dX};

  //-45°
  dX = -0.5 * steps;
  long angle45N[6] = {dX, dX, dX, dX, dX, dX};

  //-90°
  dX = -steps;
  long angle90N[6] = {dX, dX, dX, dX, dX, dX};




}//setup


//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//                            LOOP
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void loop()
{
  //
  
  
  //read DMX input and saves to array
  readDmx(dmxValues, 12);





  //*******************************
  // Choosing program mode by using DMX channel 7
  //*******************************
  program = dmxValues[6];
  //position fader channel 8
  posP = dmxValues[7];
  savePointer = dmxValues[10];
  
  //program 1 Rotates in pos or negative Direction with fader to speed
  //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  if ( !saveMode()) {
    //program 1
    if (program != 0 && program < 32) {
      steppers.runSpeed();
      rotateAll();
    }
    //program 2
    if ( program >= 32 && program < 64) {
      runPositions();
      outputLCD();
      updateLCD();
     
    }

    //program 3
    if (program >= 64 && program < 96) {
      calculatePositions(dmxAngle, dmxValues, 6);
      steppers.moveTo(dmxAngle);
      steppers.run();
      outputLCD();
      updateLCD();

    }



  }//program mode

  //*******************************
  // Save positions mode
  //*******************************

  //Channel 12 on 255 saves the Position to the save slot adress choosen on Channel 11
  //The Adress has to be choosen first

  if (saveMode()){
    if (savePointer != 0 && savePointer < 32 ) {
     steppers.getCurrentPosition(position1);
  }
//   if (savePointer >= 32 && savePointer < 64 ) {
//      steppers.getCurrentPosition(position2);
//  }
//     if (savePointer >= 64 && savePointer < 96 ) {
//      steppers.getCurrentPosition(position3);
//  }
   if (savePointer >= 254 ) {
     setZero();
  }
    
  
  
    outputLCD();
    updateLCD();
  
  }

}//loop
